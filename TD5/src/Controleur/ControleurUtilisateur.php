<?php
    namespace App\Covoiturage\Controleur;

    use App\Covoiturage\Modele\ModeleUtilisateur;

    class ControleurUtilisateur {
        // Déclaration de type de retour void : la fonction ne retourne pas de valeur
        public static function afficherListe() : void {
            $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
            self::afficherVue('vueGenerale.php', [
                'titre' => "Liste des utilisateurs",
                'cheminCorpsVue' => 'utilisateur/liste.php',
                'utilisateurs' => $utilisateurs
            ]);
        }

        public static function afficherDetail($login) : void {
            $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($login); //appel au modèle pour gérer la BD
            if (isset($utilisateur)) {
                self::afficherVue('vueGenerale.php', [
                    'titre' => "Detail utilisateur",
                    'cheminCorpsVue' => 'utilisateur/detail.php',
                    "utilisateur" => $utilisateur
                ]);
            } else {
                self::afficherVue('vueGenerale.php', [
                    'titre' => "Erreur",
                    'cheminCorpsVue' => "utilisateur/erreur.php",
                ]);
            }
        }

        public static function afficherFormulaireCreation() : void {
            self::afficherVue('vueGenerale.php', [
                'titre' => "Formulaire de création utilisateur",
                'cheminCorpsVue' => 'utilisateur/formulaireCreation.php',
            ]);
        }

        public static function creerDepuisFormulaire($login, $nom, $prenom) : void {
            $utilisateur = new ModeleUtilisateur($login, $nom, $prenom);
            $utilisateur->ajouter();
            $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
            self::afficherVue('vueGenerale.php', [
                'titre' => "Liste des utilisateurs",
                'cheminCorpsVue' => 'utilisateur/utilisateurCree.php',
                'utilisateurs' => $utilisateurs
            ]);

        }

        private static function afficherVue(string $cheminVue, array $parametres = []) : void {
            extract($parametres); // Crée des variables à partir du tableau $parametres
            require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
        }
    }
?>