<?php
/** @var ModeleUtilisateur $utilisateur */
$loginHTML = htmlspecialchars($utilisateur->getLogin());
$prenomHTML = htmlspecialchars($utilisateur->getPrenom());
$nomHTML = htmlspecialchars($utilisateur->getNom());
echo '<p> Utilisateur de login ' . $loginHTML . ' de nom ' . $nomHTML . ' et de prénom ' . $prenomHTML .'.</p>';
?>