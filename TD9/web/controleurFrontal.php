<?php
    require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

    use App\Covoiturage\Controleur\ControleurGenerique;
    use App\Covoiturage\Lib\PreferenceControleur;

    // initialisation en activant l'affichage de débogage
    $chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
    $chargeurDeClasse->register();
    // enregistrement d'une association "espace de nom" → "dossier"
    $chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

    // On récupère le controleur passée dans l'URL
    $controleur = $_REQUEST['controleur'] ?? (PreferenceControleur::lire() ?: 'utilisateur');

    // On récupère le nom de la classe du controleur
    $nomDeClasseControleur = 'App\Covoiturage\Controleur\Controleur' . ucfirst($controleur);
    if (class_exists($nomDeClasseControleur)) {
        $action = $_REQUEST['action'] ?? 'afficherListe';
        if (!in_array($action, get_class_methods($nomDeClasseControleur))) {
            $action = 'afficherErreur';
        }

        // Utilisation de la réflexion pour récupérer les paramètres attendus par l'action
        $reflecteur = new ReflectionMethod($nomDeClasseControleur, $action);
        $parametresRequis = $reflecteur->getParameters();

        // Préparer un tableau ordonné pour stocker les paramètres à envoyer
        $parametresAEnvoyer = [];

        // Remplissage des paramètres dans l'ordre en fonction de leur présence dans $_REQUEST
        foreach ($parametresRequis as $parametre) {
            $nomParametre = $parametre->getName();

            // Si le paramètre est dans $_REQUEST, on l'ajoute. Sinon, on passe null ou sa valeur par défaut si elle existe.
            if (isset($_REQUEST[$nomParametre])) {
                $parametresAEnvoyer[] = $_REQUEST[$nomParametre];
            } elseif ($parametre->isDefaultValueAvailable()) {
                $parametresAEnvoyer[] = $parametre->getDefaultValue();
            } else {
                $parametresAEnvoyer[] = null;  // Si aucune valeur n'est disponible, on passe `null`
            }
        }

        // Appel de la méthode du contrôleur avec uniquement les paramètres requis
        call_user_func_array([$nomDeClasseControleur, $action], $parametresAEnvoyer);
    } else {
        ControleurGenerique::afficherErreur("Ce controleur n'existe pas");
    }