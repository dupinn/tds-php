<?php
use App\Covoiturage\Modele\DataObject\Trajet;

/** @var Trajet $trajet */
$idHTML = htmlspecialchars($trajet->getId());
$departHTML = htmlspecialchars($trajet->getDepart());
$arriveeHTML = htmlspecialchars($trajet->getArrivee());
$dateHTML = htmlspecialchars($trajet->getDate()->format("Y-m-d"));
$prixHTML = htmlspecialchars($trajet->getPrix());
$conducteurLoginHTML = htmlspecialchars($trajet->getConducteur()->getLogin());
$nonFumeurHTML = htmlspecialchars($trajet->isNonFumeur());
?>
<form method="get" action="../web/controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <input type='hidden' name='controleur' value='trajet'>
        <input type='hidden' name='action' value='mettreAJour'>
        <input type='hidden' name='id' value="<?= $idHTML ?>">
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="depart_id">Depart&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?= $departHTML ?>" name="depart" id="depart_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="arrivee_id">Arrivée&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?= $arriveeHTML ?>" name="arrivee" id="arrivee_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="date_id">Date&#42;</label>
            <input type="date" value="<?= $dateHTML ?>" name="date" id="date_id"  required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prix_id">Prix&#42;</label>
            <input class="InputAddOn-field" type="number" value="<?= $prixHTML ?>" name="prix" id="prix_id"  required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="conducteurLogin_id">Login du conducteur&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?= $conducteurLoginHTML ?>" name="conducteurLogin" id="conducteurLogin_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nonFumeur_id">Non Fumeur ?&#42;</label>
            <input type="checkbox" name="nonFumeur" id="nonFumeur_id" <?php if ($nonFumeurHTML) echo "checked" ?>/>
        </p>
        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>