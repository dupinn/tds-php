<?php
use App\Covoiturage\Modele\DataObject\Utilisateur;

/** @var Utilisateur[] $utilisateurs */

foreach ($utilisateurs as $utilisateur) {
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    $loginURL = rawurlencode($utilisateur->getLogin());
    echo 'Utilisateur de login ' . $loginHTML;
    echo '<span style="margin-right: 40px"></span><a style="color: green" href="controleurFrontal.php?controleur=utilisateur&action=afficherDetail&login=' . $loginURL    . '" > Afficher détail </a>';
    echo "<hr>";
}
echo '<a style="color: darkviolet" href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireCreation" >  Créer utilisateur </a>';