<?php
use App\Covoiturage\Modele\DataObject\Passager;

/** @var Passager $passager */
$loginHTML = htmlspecialchars($passager->getPassagerLogin());
$idHTML = htmlspecialchars($passager->getTrajetId());

echo "<p> Le passager de login " . $loginHTML . " a bien été inscris au trajet qui a comme ID " . $idHTML . " </p>";
require_once __DIR__ . "/../trajet/liste.php";