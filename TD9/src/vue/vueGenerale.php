<?php

use \App\Covoiturage\Lib\ConnexionUtilisateur;

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../ressources/css/style.css">
        <title>
            <?php
            /**
             * @var string $titre
             */
            echo $titre;
            ?>
        </title>
    </head>
    <body>
        <header>
            <nav>
                <ul>
                    <li>
                        <a href="controleurFrontal.php?controleur=utilisateur&action=afficherListe">Gestion des utilisateurs</a>
                    </li>
                    <li>
                        <a href="controleurFrontal.php?controleur=trajet&action=afficherListe">Gestion des trajets</a>
                    </li>
                    <?php if (!ConnexionUtilisateur::estConnecte()): ?>
                        <li>
                            <a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireCreation">
                                <img src="../../ressources/img/register.png" alt="inscrire">
                            </a>
                        </li>
                        <li>
                            <a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireConnexion">
                                <img src="../../ressources/img/login.png" alt="connexion">
                            </a>
                        </li>
                    <?php else: ?>
                        <li>
                            <a href="controleurFrontal.php?controleur=utilisateur&action=afficherDetail&login=<?= rawurlencode(ConnexionUtilisateur::getLoginUtilisateurConnecte()) ?>">
                                <img src="../../ressources/img/profile.png" alt="profile">
                            </a>
                        </li>
                        <li>
                            <a href="controleurFrontal.php?controleur=utilisateur&action=deconnecter">
                                <img src="../../ressources/img/logout.png" alt="deconnexion">
                            </a>
                        </li>
                    <?php endif ?>
                    <li>
                        <a href="controleurFrontal.php?action=afficherFormulairePreference">
                            <img src="../../ressources/img/preference.png" alt="preference">
                        </a>
                    </li>
                </ul>
            </nav>
            <div>
                <?php
                /** @var string[][] $messagesFlash */
                foreach($messagesFlash as $type => $messagesFlashPourUnType) {
                    // $type est l'une des valeurs suivantes : "success", "info", "warning", "danger"
                    // $messagesFlashPourUnType est la liste des messages flash d'un type
                    foreach ($messagesFlashPourUnType as $messageFlash) {
                        echo <<< HTML
                        <div class="alert alert-$type">
                            $messageFlash
                        </div>
                        HTML;
                    }
                }
                ?>
            </div>
        </header>
        <main>
            <?php
            /**
             * @var string $cheminCorpsVue
             */
            require __DIR__ . "/{$cheminCorpsVue}";
            ?>
        </main>
        <footer>
            <p>
                Site de covoiturage de ...
            </p>
        </footer>
    </body>
</html>