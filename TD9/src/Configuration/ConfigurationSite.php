<?php

namespace App\Covoiturage\Configuration;

class ConfigurationSite
{
    static private array $configurationSite = array(
        'dureeExpirationSession' => 1800,
        'urlAbsolue' => 'http://localhost/tds-php/TD9/web/controleurFrontal.php'
    );

    static public function getDureeExpiration() : int {
        return ConfigurationSite::$configurationSite['dureeExpirationSession'];
    }

    static public function getURLAbsolue() : string {
        return ConfigurationSite::$configurationSite['urlAbsolue'];
    }

    static public function getDebug() : bool {
        return true;
    }
}