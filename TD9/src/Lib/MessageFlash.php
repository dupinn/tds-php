<?php

namespace App\Covoiturage\Lib;

use App\Covoiturage\Modele\HTTP\Session;

class MessageFlash
{

    // Les messages sont enregistrés en session associée à la clé suivante
    private static string $cleFlash = "_messagesFlash";

    // $type parmi "success", "info", "warning" ou "danger"
    public static function ajouter(string $type, string $message): void
    {
        $messages = Session::getInstance()->lire(self::$cleFlash) ?? [];
        $messages[$type][] = $message;
        Session::getInstance()->enregistrer(self::$cleFlash, $messages);
    }

    public static function contientMessage(string $type): bool
    {
        $messages = Session::getInstance()->lire(self::$cleFlash);
        return isset($messages[$type]);
    }

    // Attention : la lecture doit détruire le message
    public static function lireMessages(string $type): array
    {
        $messages = Session::getInstance()->lire(self::$cleFlash);
        if (self::contientMessage($type)) {
            $messagesType = $messages[$type];
            unset($messages[$type]);
            Session::getInstance()->enregistrer(self::$cleFlash, $messages);
            return $messagesType;
        }
        return [];
    }

    public static function lireTousMessages() : array
    {
        $messages = Session::getInstance()->lire(self::$cleFlash);
        if ($messages) {
            Session::getInstance()->supprimer(self::$cleFlash);
            return $messages;
        }
        return [];
    }

}