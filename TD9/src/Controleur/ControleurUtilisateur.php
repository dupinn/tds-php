<?php
    namespace App\Covoiturage\Controleur;

    use App\Covoiturage\Lib\ConnexionUtilisateur;
    use App\Covoiturage\Lib\MessageFlash;
    use App\Covoiturage\Lib\MotDePasse;
    use App\Covoiturage\Lib\VerificationEmail;
    use App\Covoiturage\Modele\Repository\UtilisateurRepository;
    use App\Covoiturage\Modele\DataObject\Utilisateur;

    class ControleurUtilisateur extends ControleurGenerique
    {
        // Déclaration de type de retour void : la fonction ne retourne pas de valeur
        public static function afficherListe(): void
        {
            $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
            self::afficherVue('vueGenerale.php', [
                'titre' => "Liste des utilisateurs",
                'cheminCorpsVue' => 'utilisateur/liste.php',
                'utilisateurs' => $utilisateurs
            ]);
        }

        public static function afficherDetail($login): void
        {
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login); //appel au modèle pour gérer la BD
            if (isset($utilisateur)) {
                self::afficherVue('vueGenerale.php', [
                    'titre' => "Detail utilisateur",
                    'cheminCorpsVue' => 'utilisateur/detail.php',
                    "utilisateur" => $utilisateur
                ]);
            } else {
                MessageFlash::ajouter('danger', 'Login inconnu');
                self::redirectionVersURL('controleurFrontal.php?controleur=utilisateur&action=afficherListe');
            }
        }

        public static function afficherFormulaireCreation(): void
        {
            self::afficherVue('vueGenerale.php', [
                'titre' => "Formulaire de création utilisateur",
                'cheminCorpsVue' => 'utilisateur/formulaireCreation.php',
            ]);
        }

        public static function afficherFormulaireMiseAJour($login): void
        {
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
            if (ConnexionUtilisateur::estUtilisateur($login) || (ConnexionUtilisateur::estAdministrateur() && isset($utilisateur))) {
                self::afficherVue('vueGenerale.php', [
                    'titre' => "Formulaire de mise à jour utilisateur",
                    'cheminCorpsVue' => 'utilisateur/formulaireMiseAJour.php',
                    'utilisateur' => $utilisateur
                ]);
                return;
            }
            if (ConnexionUtilisateur::estAdministrateur() || !isset($utilisateur)) {
                MessageFlash::ajouter('warning', "Login inconnu");
            } else if (!ConnexionUtilisateur::estUtilisateur($login)) {
                MessageFlash::ajouter('danger', "La mise à jour n'est possible que pour l'utilisateur concerné");
            }
            self::redirectionVersURL("controleurFrontal.php?controleur=utilisateur&action=afficherListe");
        }

        public static function afficherFormulaireConnexion(): void
        {
            self::afficherVue('vueGenerale.php', [
                'titre' => "Formulaire de connexion utilisateur",
                'cheminCorpsVue' => 'utilisateur/formulaireConnexion.php',
            ]);
        }

        private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur {
            return new Utilisateur($tableauDonneesFormulaire['login'],
                $tableauDonneesFormulaire['nom'],
                $tableauDonneesFormulaire['prenom'],
                "",
                $tableauDonneesFormulaire['email'],
                MotDePasse::genererChaineAleatoire(),
                MotDePasse::hacher($tableauDonneesFormulaire['mdp']),
                isset($tableauDonneesFormulaire['estAdmin'])
            );
        }

        public static function creerDepuisFormulaire(): void
        {
            if ($_REQUEST['mdp'] === $_REQUEST['mdp2']) {
                if (!filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL)) {
                    self::afficherErreur("L'email n'est pas valide");
                    return;
                }
                $utilisateur = self::construireDepuisFormulaire($_REQUEST);
                if (!ConnexionUtilisateur::estAdministrateur()) {
                    $utilisateur->setAdmin(false);
                }
                (new UtilisateurRepository())->ajouter($utilisateur);
                VerificationEmail::envoiEmailValidation($utilisateur);
                MessageFlash::ajouter('success', "L'utilisateur a bien été créé");
                self::redirectionVersURL("controleurFrontal.php?controleur=utilisateur&action=afficherListe");
            } else {
                MessageFlash::ajouter('warning', "Mots de passe distincts");
                self::redirectionVersURL("controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireCreation");
            }
        }

        public static function validerEmail() : void {
            $login = rawurldecode($_REQUEST['login']);
            $nonce = rawurldecode($_REQUEST['nonce']);
            if (VerificationEmail::traiterEmailValidation($login, $nonce)) {
                MessageFlash::ajouter('success', "Email vérifié");
                self::redirectionVersURL("controleurFrontal.php?controleur=utilisateur&action=afficherDetail&login=$login");
            } else {
                MessageFlash::ajouter('warning', "Echec de la vérification du mail");
                self::redirectionVersURL("controleurFrontal.php?controleur=utilisateur&action=afficherListe");
            }
        }

        public static function supprime($login): void
        {
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']);
            if (isset($utilisateur)) {
                if (ConnexionUtilisateur::estUtilisateur($login) || ConnexionUtilisateur::estAdministrateur()) {
                    (new UtilisateurRepository())->supprimer($login);
                    MessageFlash::ajouter('success', "L'utilisateur a bien été supprimé");
                    self::redirectionVersURL("controleurFrontal.php?controleur=utilisateur&action=afficherListe");
                }
            }
        }

        public static function mettreAJour(): void
        {
            if (isset($_REQUEST['login']) && isset($_REQUEST['nom']) && isset($_REQUEST['prenom']) && isset($_REQUEST['ancien_mdp']) && isset($_REQUEST['mdp']) && isset($_REQUEST['mdp2'])) {
                /** @var Utilisateur $utilisateur */
                $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']);
                if (ConnexionUtilisateur::estUtilisateur($utilisateur->getLogin()) || (ConnexionUtilisateur::estAdministrateur() && isset($utilisateur))) {
                    if ($_REQUEST['mdp'] === $_REQUEST['mdp2']) {
                        if (ConnexionUtilisateur::estAdministrateur() || MotDePasse::verifier($_REQUEST['ancien_mdp'], $utilisateur->getMdpHache())) {
                            if ($utilisateur->getEmail() !== $_REQUEST['email'] && filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL)) {
                                $utilisateur->setEmailAValider($_REQUEST['email']);
                                $utilisateur->setNonce(MotDePasse::genererChaineAleatoire());
                                VerificationEmail::envoiEmailValidation($utilisateur);
                            }
                            $utilisateur->setLogin($_REQUEST['login']);
                            $utilisateur->setNom($_REQUEST['nom']);
                            $utilisateur->setPrenom($_REQUEST['prenom']);
                            $utilisateur->setMdpHache(MotDePasse::hacher($_REQUEST['mdp']));
                            $utilisateur->setAdmin(ConnexionUtilisateur::estAdministrateur() && isset($_REQUEST['estAdmin']));
                            (new UtilisateurRepository())->mettreAJour($utilisateur);
                            MessageFlash::ajouter('success', "L'utilisateur a bien été mis à jour");
                            self::redirectionVersURL("controleurFrontal.php?controleur=utilisateur&action=afficherListe");
                        } else if (!MotDePasse::verifier($_REQUEST['ancien_mdp'], $utilisateur->getMdpHache())) {
                            MessageFlash::ajouter('warning', "Ancien mot de passe erroné");
                            self::redirectionVersURL("controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login={$utilisateur->getLogin()}");
                        }
                    } else {
                        MessageFlash::ajouter('warning', "Mots de passe distincts");
                        self::redirectionVersURL("controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login={$utilisateur->getLogin()}");
                    }
                } else if (ConnexionUtilisateur::estAdministrateur()) {
                    MessageFlash::ajouter('warning', "Login inconnu");
                    self::redirectionVersURL("controleurFrontal.php?controleur=utilisateur&action=afficherListe");
                } else if (!ConnexionUtilisateur::estUtilisateur($utilisateur->getLogin())) {
                    MessageFlash::ajouter('danger', "La mise à jour n'est possible que pour l'utilisateur concerné");
                    self::redirectionVersURL("controleurFrontal.php?controleur=utilisateur&action=afficherListe");
                }
            }
        }

        public static function connecter() : void {
            $login = $_REQUEST['login'];
            $mdp = $_REQUEST['mdp'];
            if (isset($login) && isset($mdp)) {
                /** @var Utilisateur $utilisateur */
                $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']);
                if (isset($utilisateur) && MotDePasse::verifier($_REQUEST['mdp'], $utilisateur->getMdpHache()) && VerificationEmail::aValideEmail($utilisateur)) {
                    ConnexionUtilisateur::connecter($login);
                    MessageFlash::ajouter('success', "L'utilisateur a bien été connecté");
                    $loginURL = rawurlencode($login);
                    self::redirectionVersURL("controleurFrontal.php?controleur=utilisateur&action=afficherDetail&login=$loginURL");
                    return;
                } else if (!isset($utilisateur)) {
                    MessageFlash::ajouter('warning', "Login inconnu");
                } else {
                    MessageFlash::ajouter('warning', "Mot de passe incorrect");
                }
            } else {
                MessageFlash::ajouter('danger', "Login et/ou mot de passe manquant");
            }
            self::redirectionVersURL("controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireConnexion");
        }

        public static function deconnecter() {
            ConnexionUtilisateur::deconnecter();
            MessageFlash::ajouter('success', "L'utilisateur a bien été déconnecté");
            self::redirectionVersURL("controleurFrontal.php?controleur=utilisateur&action=afficherListe");
        }

        public static function afficherErreur(string $messageErreur = "") : void
        {
            self::afficherVue('vueGenerale.php', [
                'titre' => 'Erreur utilisateur',
                'cheminCorpsVue' => 'utilisateur/erreur.php',
                'messageErreur' => $messageErreur
            ]);
        }
    }