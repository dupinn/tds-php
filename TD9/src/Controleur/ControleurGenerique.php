<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\MessageFlash;
use App\Covoiturage\Lib\PreferenceControleur;

class ControleurGenerique
{
    public static function enregistrerPreference() {
        $controleur = $_REQUEST["controleur_defaut"];
        PreferenceControleur::enregistrer($controleur);
        MessageFlash::ajouter('success', "La préférence de contrôleur est enregistrée");
        self::redirectionVersURL("controleurFrontal.php?controleur=$controleur");
    }

    public static function afficherFormulairePreference() {
        self::afficherVue('vueGenerale.php', [
            'titre' => 'Formulaire preference',
            'cheminCorpsVue' => 'formulairePreference.php'
        ]);
    }

    public static function redirectionVersURL($url) : void {
        header("Location: $url");
        exit();
    }

    public static function afficherErreur(string $messageErreur = "") : void
    {
        self::afficherVue('vueGenerale.php', [
            'titre' => 'Erreur générique',
            'cheminCorpsVue' => 'erreur.php',
            'messageErreur' => $messageErreur
        ]);
    }

    protected static function afficherVue(string $cheminVue, array $parametres = []) : void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        $messagesFlash = MessageFlash::lireTousMessages();
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }
}