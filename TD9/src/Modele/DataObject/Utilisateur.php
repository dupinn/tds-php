<?php
    namespace App\Covoiturage\Modele\DataObject;

    use App\Covoiturage\Modele\Repository\UtilisateurRepository;

    class Utilisateur extends AbstractDataObject
    {
        private string $login;
        private string $nom;
        private string $prenom;
        private string $email;
        private string $emailAValider;
        private string $nonce;
        private string $mdpHache;
        private bool $estAdmin;
        /**
         * @var Trajet[]|null
         */
        private ?array $trajetsCommePassager;

        public function __construct(string $login, string $nom, string $prenom, string $email, string $emailAValider, string $nonce, string $mdpHache, bool $estAdmin) {
            $this->login = substr($login, 0, 64);
            $this->nom = $nom;
            $this->prenom = $prenom;
            $this->email = $email;
            $this->emailAValider = $emailAValider;
            $this->nonce = $nonce;
            $this->mdpHache = $mdpHache;
            $this->trajetsCommePassager = null;
            $this->estAdmin = $estAdmin;
        }

        // un getter
        public function getNom() : string {
            return $this->nom;
        }

        // un setter
        public function setNom(string $nom) {
            $this->nom = $nom;
        }

        /**
         * @return mixed
         */
        public function getLogin() : string {
            return $this->login;
        }

        /**
         * @param mixed $login
         */
        public function setLogin(string $login) {
            $this->login = substr($login, 0, 64);
        }

        /**
         * @return mixed
         */
        public function getPrenom() : string {
            return $this->prenom;
        }

        /**
         * @param mixed $prenom
         */
        public function setPrenom(string $prenom) {
            $this->prenom = $prenom;
        }

        public function getEmail(): string
        {
            return $this->email;
        }

        public function setEmail(string $email): void
        {
            $this->email = $email;
        }

        public function getEmailAValider(): string
        {
            return $this->emailAValider;
        }

        public function setEmailAValider(string $emailAValider): void
        {
            $this->emailAValider = $emailAValider;
        }

        public function getNonce(): string
        {
            return $this->nonce;
        }

        public function setNonce(string $nonce): void
        {
            $this->nonce = $nonce;
        }

        public function getTrajetsCommePassager(): ?array
        {
            return $this->trajetsCommePassager ?? (new UtilisateurRepository())->recupererTrajetsCommePassager($this);
        }

        public function setTrajetsCommePassager(?array $trajetsCommePassager): void
        {
            $this->trajetsCommePassager = $trajetsCommePassager;
        }

        public function getMdpHache(): string
        {
            return $this->mdpHache;
        }

        public function setMdpHache(string $mdpHache): void
        {
            $this->mdpHache = $mdpHache;
        }

        public function estAdmin(): bool
        {
            return $this->estAdmin;
        }

        public function setAdmin(bool $estAdmin): void
        {
            $this->estAdmin = $estAdmin;
        }

    }