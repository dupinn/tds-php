<?php

namespace App\Covoiturage\Modele\DataObject;

class Passager extends AbstractDataObject
{
    private string $trajetId;
    private string $passagerLogin;

    public function __construct(string $trajetId, string $passagerLogin) {
        $this->trajetId = $trajetId;
        $this->passagerLogin = $passagerLogin;
    }

    public function getTrajetId(): string
    {
        return $this->trajetId;
    }

    public function setTrajetId(string $trajetId): void
    {
        $this->trajetId = $trajetId;
    }

    public function getPassagerLogin(): string
    {
        return $this->passagerLogin;
    }

    public function setPassagerLogin(string $passagerLogin): void
    {
        $this->passagerLogin = $passagerLogin;
    }
}