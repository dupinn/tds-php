<?php

namespace App\Covoiturage\Modele\HTTP;

class Cookie
{
    public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = null): void {
        if (isset($dureeExpiration)) {
            setcookie($cle, serialize($valeur), $dureeExpiration);
        } else {
            setcookie($cle, serialize($valeur), 0);
        }
    }

    public static function lire(string $cle): mixed {
        if (isset($_COOKIE[$cle])) {
            return unserialize($_COOKIE[$cle]);
        }
        return null;
    }

    public static function contient($cle) : bool {
        if (isset($_COOKIE[$cle])) {
            return true;
        }
        return false;
    }

    public static function supprimer($cle) : void {
        if (isset($_COOKIE[$cle])) {
            setcookie($cle, serialize($_COOKIE[$cle]), 1);
            unset($_COOKIE[$cle]);
        }
    }
}