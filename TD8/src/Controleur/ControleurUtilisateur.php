<?php
    namespace App\Covoiturage\Controleur;

    use App\Covoiturage\Lib\ConnexionUtilisateur;
    use App\Covoiturage\Lib\MotDePasse;
    use App\Covoiturage\Lib\VerificationEmail;
    use App\Covoiturage\Modele\Repository\UtilisateurRepository;
    use App\Covoiturage\Modele\DataObject\Utilisateur;

    class ControleurUtilisateur extends ControleurGenerique
    {
        // Déclaration de type de retour void : la fonction ne retourne pas de valeur
        public static function afficherListe(): void
        {
            $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
            self::afficherVue('vueGenerale.php', [
                'titre' => "Liste des utilisateurs",
                'cheminCorpsVue' => 'utilisateur/liste.php',
                'utilisateurs' => $utilisateurs
            ]);
        }

        public static function afficherDetail($login): void
        {
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login); //appel au modèle pour gérer la BD
            if (isset($utilisateur)) {
                self::afficherVue('vueGenerale.php', [
                    'titre' => "Detail utilisateur",
                    'cheminCorpsVue' => 'utilisateur/detail.php',
                    "utilisateur" => $utilisateur
                ]);
            } else {
                self::afficherErreur("Cette utilisateur n'existe pas");
            }
        }

        public static function afficherFormulaireCreation(): void
        {
            self::afficherVue('vueGenerale.php', [
                'titre' => "Formulaire de création utilisateur",
                'cheminCorpsVue' => 'utilisateur/formulaireCreation.php',
            ]);
        }

        public static function afficherFormulaireMiseAJour($login): void
        {
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
            if (ConnexionUtilisateur::estUtilisateur($login) || (ConnexionUtilisateur::estAdministrateur() && isset($utilisateur))) {
                self::afficherVue('vueGenerale.php', [
                    'titre' => "Formulaire de mise à jour utilisateur",
                    'cheminCorpsVue' => 'utilisateur/formulaireMiseAJour.php',
                    'utilisateur' => $utilisateur
                ]);
                return;
            }
            if (ConnexionUtilisateur::estAdministrateur()) {
                self::afficherErreur("Login inconnu");
            } else if (!ConnexionUtilisateur::estUtilisateur($login)) {
                self::afficherErreur("La mise à jour n'est possible que pour l'utilisateur connecté");
            }
        }

        public static function afficherFormulaireConnexion(): void
        {
            self::afficherVue('vueGenerale.php', [
                'titre' => "Formulaire de connexion utilisateur",
                'cheminCorpsVue' => 'utilisateur/formulaireConnexion.php',
            ]);
        }

        private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur {
            return new Utilisateur($tableauDonneesFormulaire['login'],
                $tableauDonneesFormulaire['nom'],
                $tableauDonneesFormulaire['prenom'],
                "",
                $tableauDonneesFormulaire['email'],
                MotDePasse::genererChaineAleatoire(),
                MotDePasse::hacher($tableauDonneesFormulaire['mdp']),
                isset($tableauDonneesFormulaire['estAdmin'])
            );
        }

        public static function creerDepuisFormulaire(): void
        {
            if ($_REQUEST['mdp'] === $_REQUEST['mdp2']) {
                if (!filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL)) {
                    self::afficherErreur("L'email n'est pas valide");
                    return;
                }
                $utilisateur = self::construireDepuisFormulaire($_REQUEST);
                if (!ConnexionUtilisateur::estAdministrateur()) {
                    $utilisateur->setAdmin(false);
                }
                (new UtilisateurRepository())->ajouter($utilisateur);
                VerificationEmail::envoiEmailValidation($utilisateur);
                $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
                self::afficherVue('vueGenerale.php', [
                    'titre' => "Liste des utilisateurs",
                    'cheminCorpsVue' => 'utilisateur/utilisateurCree.php',
                    'utilisateurs' => $utilisateurs
                ]);
            } else {
                self::afficherErreur("Mots de passe distincts");
            }
        }

        public static function validerEmail() : void {
            $login = rawurldecode($_REQUEST['login']);
            $nonce = rawurldecode($_REQUEST['nonce']);
            if (isset($login) && isset($nonce) && VerificationEmail::traiterEmailValidation($login, $nonce)) {
                self::afficherDetail($login);
            } else {
                self::afficherErreur("La vérification du mail à échoué");
            }
        }

        public static function supprime($login): void
        {
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']);
            if (isset($utilisateur)) {
                if (ConnexionUtilisateur::estUtilisateur($login) || ConnexionUtilisateur::estAdministrateur()) {
                    (new UtilisateurRepository())->supprimer($login);
                    $utilisateurs = (new UtilisateurRepository())->recuperer();
                    self::afficherVue('vueGenerale.php', [
                        'titre' => "Liste des utilisateurs",
                        'cheminCorpsVue' => 'utilisateur/utilisateurSupprime.php',
                        'login' => $login,
                        'utilisateurs' => $utilisateurs
                    ]);
                }
            }
        }

        public static function mettreAJour(): void
        {
            if (isset($_REQUEST['login']) && isset($_REQUEST['nom']) && isset($_REQUEST['prenom']) && isset($_REQUEST['ancien_mdp']) && isset($_REQUEST['mdp']) && isset($_REQUEST['mdp2'])) {
                /** @var Utilisateur $utilisateur */
                $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']);
                if (ConnexionUtilisateur::estUtilisateur($utilisateur->getLogin()) || (ConnexionUtilisateur::estAdministrateur() && isset($utilisateur))) {
                    if ($_REQUEST['mdp'] === $_REQUEST['mdp2']) {
                        if (ConnexionUtilisateur::estAdministrateur() || MotDePasse::verifier($_REQUEST['ancien_mdp'], $utilisateur->getMdpHache())) {
                            if ($utilisateur->getEmail() !== $_REQUEST['email'] && filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL)) {
                                $utilisateur->setEmailAValider($_REQUEST['email']);
                                $utilisateur->setNonce(MotDePasse::genererChaineAleatoire());
                                VerificationEmail::envoiEmailValidation($utilisateur);
                            }
                            $utilisateur->setLogin($_REQUEST['login']);
                            $utilisateur->setNom($_REQUEST['nom']);
                            $utilisateur->setPrenom($_REQUEST['prenom']);
                            $utilisateur->setMdpHache(MotDePasse::hacher($_REQUEST['mdp']));
                            $utilisateur->setAdmin(ConnexionUtilisateur::estAdministrateur() && isset($_REQUEST['estAdmin']));
                            (new UtilisateurRepository())->mettreAJour($utilisateur);
                            $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
                            self::afficherVue('vueGenerale.php', [
                                'titre' => "Liste des utilisateurs",
                                'cheminCorpsVue' => 'utilisateur/utilisateurMisAJour.php',
                                'login' => $utilisateur->getLogin(),
                                'utilisateurs' => $utilisateurs
                            ]);
                            return;
                        }
                    }
                } else if (ConnexionUtilisateur::estAdministrateur()) {
                    self::afficherErreur("Login inconnu");
                } else if (!ConnexionUtilisateur::estUtilisateur($utilisateur->getLogin())) {
                    self::afficherErreur("La mise à jour n'est possible que pour l'utilisateur connecté");
                }
            }
            self::afficherErreur("Infomations(s) incorrect(s)");
        }

        public static function connecter() : void {
            $login = $_REQUEST['login'];
            $mdp = $_REQUEST['mdp'];
            if (isset($login) && isset($mdp)) {
                /** @var Utilisateur $utilisateur */
                $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']);
                if (isset($utilisateur) && MotDePasse::verifier($_REQUEST['mdp'], $utilisateur->getMdpHache()) && VerificationEmail::aValideEmail($utilisateur)) {
                    ConnexionUtilisateur::connecter($login);
                    self::afficherVue('vueGenerale.php', [
                        'titre' => "Utilisateur connecté",
                        'cheminCorpsVue' => 'utilisateur/utilisateurConnecte.php',
                        'utilisateur' => $utilisateur,
                    ]);
                } else {
                    self::afficherErreur("Login et/ou mot de passe et/ou mail incorrect");
                }
            } else {
                self::afficherErreur("Login et/ou mot de passe manquant");
            }
        }

        public static function deconnecter() {
            ConnexionUtilisateur::deconnecter();
            $utilisateurs = (new UtilisateurRepository())->recuperer();
            self::afficherVue('vueGenerale.php', [
                'titre' => "Deconnexion",
                'cheminCorpsVue' => 'utilisateur/utilisateurDeconnecte.php',
                'utilisateurs' => $utilisateurs
            ]);
        }

        public static function afficherErreur(string $messageErreur = "") : void
        {
            self::afficherVue('vueGenerale.php', [
                'titre' => 'Erreur utilisateur',
                'cheminCorpsVue' => 'utilisateur/erreur.php',
                'messageErreur' => $messageErreur
            ]);
        }
    }