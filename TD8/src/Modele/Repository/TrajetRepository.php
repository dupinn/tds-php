<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Trajet;
use DateTime;

class TrajetRepository extends AbstractRepository
{
    protected function getNomTable(): string
    {
        return 'trajet';
    }

    protected function getNomClePrimaire(): string
    {
        return 'id';
    }

    protected function getNomsColonnes(): array
    {
        return ["id", "depart", "arrivee", "date", "prix", "conducteurLogin", "nonFumeur"];
    }

    protected function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]),
            $trajetTableau["prix"],
            (new UtilisateurRepository())->recupererParClePrimaire($trajetTableau["conducteurLogin"]),
            $trajetTableau["nonFumeur"],
        );
        $trajet->setPassagers(TrajetRepository::recupererPassagers($trajet));
        return $trajet;
    }

    protected function formatTableauSQL(AbstractDataObject $trajet): array
    {
        /** @var Trajet $trajet */
        $nonFumeur = $trajet->isNonFumeur() ? 1 : 0;
        return array(
            "idTag" => $trajet->getId(),
            "departTag" => $trajet->getDepart(),
            "arriveeTag" => $trajet->getArrivee(),
            "dateTag" => $trajet->getDate()->format("Y-m-d"),
            "prixTag" => $trajet->getPrix(),
            "conducteurLoginTag" => $trajet->getConducteur()->getLogin(),
            "nonFumeurTag" => $nonFumeur,
        );
    }

    public static function recupererPassagers(Trajet $trajet) : array {
        $sql = "SELECT u.login, u.nom, u.prenom, u.email, u.emailAValider, u.nonce, u.mdpHache, u.estAdmin FROM utilisateur u INNER JOIN passager p ON u.login = p.passagerLogin WHERE p.trajetId = :idTag;";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "idTag" => $trajet->getId()
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        $tableau = [];
        foreach($pdoStatement as $utilisateurFormatTableau) {
            $tableau[] = (new UtilisateurRepository())->construireDepuisTableauSQL($utilisateurFormatTableau);
        }
        return $tableau;
    }

    public function supprimerPassager(Trajet $trajet, string $passagerLogin) : bool
    {
        $sql = "DELETE FROM passager WHERE trajetId = :trajetIdTag AND passagerLogin = :passagerLoginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        if ($pdoStatement->rowCount() === 1) {
            $values = array(
                "trajetIdTag" => $trajet->getId(),
                "passagerLoginTag" => $passagerLogin
            );
            // On donne les valeurs et on exécute la requête
            return $pdoStatement->execute($values);
        }
        return false;
    }
}