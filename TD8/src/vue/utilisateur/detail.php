<?php

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;

/** @var Utilisateur $utilisateur */
if (ConnexionUtilisateur::estUtilisateur($utilisateur->getLogin()) || ConnexionUtilisateur::estAdministrateur()) {
    $loginURL = rawurlencode($utilisateur->getLogin());
    echo '<span style="margin-right: 40px"></span><a style="color: crimson" href="controleurFrontal.php?controleur=utilisateur&action=supprime&login=' . $loginURL    . '" > Supprimer </a>';
    echo '<span style="margin-right: 40px"></span><a style="color: orange" href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=' . $loginURL    . '" > Mettre à jour </a>';
}
$loginHTML = htmlspecialchars($utilisateur->getLogin());
$prenomHTML = htmlspecialchars($utilisateur->getPrenom());
$nomHTML = htmlspecialchars($utilisateur->getNom());
$mailHTML = htmlspecialchars($utilisateur->getEmail());
echo '<p> Utilisateur de login ' . $loginHTML . ', de nom ' . $nomHTML . ', de prénom ' . $prenomHTML .', et de mail ' . $mailHTML . '</p>';
echo '<p>Il sera passager sur les trajet(s) :</p>';
/** @var Trajet[] $trajets */
$trajets = $utilisateur->getTrajetsCommePassager();
if (!empty($trajets)) {
    foreach ($trajets as $trajet) {
        if (is_object($trajet)) {
            $idHTML = htmlspecialchars($trajet->getId());
            $departHTML = htmlspecialchars($trajet->getDepart());
            $arriveeHTML = htmlspecialchars($trajet->getArrivee());
            echo '<p>- Trajet qui a comme ID ' . $idHTML . ', qui aura comme depart ' . $departHTML . ' et comme arrivee ' . $arriveeHTML .'.</p>';
        } else {
            var_dump($trajet); // Ajoute ce code pour voir ce que contient réellement la variable.
            echo "Erreur : Le trajet n'est pas un objet.";
        }
    }
} else {
    echo "<p> Aucun </p>";
}