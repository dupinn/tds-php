<?php
use App\Covoiturage\Configuration\ConfigurationSite;
use App\Covoiturage\Lib\PreferenceControleur;
?>
<form method="<?= (ConfigurationSite::getDebug() ? 'get' : 'post') ?>" action="../web/controleurFrontal.php">
    <fieldset>
        <input type='hidden' name='action' value='enregistrerPreference'>
        <p>
            <label for="utilisateurId">Utilisateur</label>
            <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur" <?php if(PreferenceControleur::lire() === 'utilisateur') echo 'checked';?>>
        </p>
        <p>
            <label for="trajetId">Trajet</label>
            <input type="radio" id="trajetId" name="controleur_defaut" value="trajet" <?php if(PreferenceControleur::lire() === 'trajet') echo 'checked';?>>
        </p>
        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>