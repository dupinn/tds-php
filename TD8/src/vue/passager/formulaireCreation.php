<?php
use App\Covoiturage\Modele\DataObject\Trajet;

/** @var Trajet $trajet */
$idHTML = htmlspecialchars($trajet->getId());
?>
<form method="get" action="../web/controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <input type='hidden' name='controleur' value='passager'>
        <input type='hidden' name='action' value='creerDepuisFormulaire'>
        <input type='hidden' name='id' value='<?= $idHTML ?>'>
        <p>
            <label for="passager_id">Login du passager</label> :
            <input type="text" placeholder="leblancj" name="login" id="passager_id" required/>
        </p>
        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>