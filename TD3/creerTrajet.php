<?php
    require_once "Trajet.php";
    require_once "Utilisateur.php";
    $trajet = new Trajet(null, $_POST["depart"], $_POST["arrivee"], new DateTime($_POST["date"]), $_POST["prix"], Utilisateur::recupererUtilisateurParLogin($_POST["conducteurLogin"]), isset($_POST["nonFumeur"]));
    $trajet->ajouter();
?>
