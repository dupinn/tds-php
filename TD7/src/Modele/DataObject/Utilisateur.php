<?php
    namespace App\Covoiturage\Modele\DataObject;

    use App\Covoiturage\Modele\Repository\UtilisateurRepository;

    class Utilisateur extends AbstractDataObject
    {
        private string $login;
        private string $nom;
        private string $prenom;
        /**
         * @var Trajet[]|null
         */
        private ?array $trajetsCommePassager;

        public function __construct(string $login, string $nom, string $prenom) {
            $this->login = substr($login, 0, 64);
            $this->nom = $nom;
            $this->prenom = $prenom;
            $this->trajetsCommePassager = null;
        }

        // un getter
        public function getNom() : string {
            return $this->nom;
        }

        // un setter
        public function setNom(string $nom) {
            $this->nom = $nom;
        }

        /**
         * @return mixed
         */
        public function getLogin() : string {
            return $this->login;
        }

        /**
         * @param mixed $login
         */
        public function setLogin(string $login) {
            $this->login = substr($login, 0, 64);
        }

        /**
         * @return mixed
         */
        public function getPrenom() : string {
            return $this->prenom;
        }

        /**
         * @param mixed $prenom
         */
        public function setPrenom(string $prenom) {
            $this->prenom = $prenom;
        }

        public function getTrajetsCommePassager(): ?array
        {
            return $this->trajetsCommePassager ?? (new UtilisateurRepository())->recupererTrajetsCommePassager($this);
        }

        public function setTrajetsCommePassager(?array $trajetsCommePassager): void
        {
            $this->trajetsCommePassager = $trajetsCommePassager;
        }
    }