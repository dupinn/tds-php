<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Passager;

class PassagerRepository extends AbstractRepository
{
    protected function getNomTable(): string
    {
        return 'passager';
    }

    protected function getNomClePrimaire(): string
    {
        return 'trajetId,passagerLogin';
    }

    protected function getNomsColonnes(): array
    {
        return ["trajetId", "passagerLogin"];
    }

    protected function construireDepuisTableauSQL(array $passagerFormatTableau): Passager
    {
        return new Passager($passagerFormatTableau['trajetId'], $passagerFormatTableau['passagerLogin']);
    }

    protected function formatTableauSQL(AbstractDataObject $passager): array
    {
        /** @var Passager $passager */
        return array(
            "trajetIdTag" => $passager->getTrajetId(),
            "passagerLoginTag" => $passager->getPassagerLogin(),
        );
    }
}