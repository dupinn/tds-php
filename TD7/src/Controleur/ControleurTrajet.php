<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateTime;

class ControleurTrajet extends ControleurGenerique
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $trajets = (new TrajetRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', [
            'titre' => "Liste des trajets",
            'cheminCorpsVue' => 'trajet/liste.php',
            'trajets' => $trajets
        ]);
    }

    public static function afficherDetail($id): void
    {
        $trajet = (new TrajetRepository())->recupererParClePrimaire($id); //appel au modèle pour gérer la BD
        if (isset($trajet)) {
            self::afficherVue('vueGenerale.php', [
                'titre' => "Detail trajet",
                'cheminCorpsVue' => 'trajet/detail.php',
                "trajet" => $trajet
            ]);
        } else {
            self::afficherErreur("Ce trajet n'existe pas");
        }
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php', [
            'titre' => "Formulaire de création trajet",
            'cheminCorpsVue' => 'trajet/formulaireCreation.php',
        ]);
    }

    public static function afficherFormulaireMiseAJour($id): void
    {
        $trajet = (new TrajetRepository())->recupererParClePrimaire($id);
        if (isset($trajet)) {
            self::afficherVue('vueGenerale.php', [
                'titre' => "Formulaire de mise à jour trajet",
                'cheminCorpsVue' => 'trajet/formulaireMiseAJour.php',
                'trajet' => $trajet
            ]);
        } else {
            self::afficherErreur("Ce trajet n'existe pas");
        }
    }

    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Trajet {
        return new Trajet($tableauDonneesFormulaire['id'] ?? null,
            $tableauDonneesFormulaire['depart'],
            $tableauDonneesFormulaire['arrivee'],
            new DateTime($tableauDonneesFormulaire['date']),
            $tableauDonneesFormulaire['prix'],
            (new UtilisateurRepository())->recupererParClePrimaire($tableauDonneesFormulaire['conducteurLogin']),
            isset($tableauDonneesFormulaire['nonFumeur']),
        );
    }

    public static function creerDepuisFormulaire(): void
    {
        $trajet = self::construireDepuisFormulaire($_GET);
        (new TrajetRepository())->ajouter($trajet);
        $trajets = (new TrajetRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', [
            'titre' => "Liste des trajets",
            'cheminCorpsVue' => 'trajet/trajetCree.php',
            'trajets' => $trajets
        ]);
    }

    public static function supprime($id): void
    {
        (new TrajetRepository())->supprimer($id);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue('vueGenerale.php', [
            'titre' => "Liste des trajets",
            'cheminCorpsVue' => 'trajet/trajetSupprime.php',
            'id' => $id,
            'trajets' => $trajets
        ]);
    }

    public static function mettreAJour(): void
    {
        $trajet = self::construireDepuisFormulaire($_GET);
        (new TrajetRepository())->mettreAJour($trajet);
        $trajets = (new TrajetRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', [
            'titre' => "Liste des trajets",
            'cheminCorpsVue' => 'trajet/trajetMisAJour.php',
            'id' => $trajet->getId(),
            'trajets' => $trajets
        ]);
    }

    public static function supprimerPassager($id, $login): void
    {
        /** @var Trajet $trajet */
        $trajet = (new TrajetRepository())->recupererParClePrimaire($id);
        (new TrajetRepository())->supprimerPassager($trajet, $login);
        $trajets = (new TrajetRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', [
            'titre' => "Liste des trajets",
            'cheminCorpsVue' => 'trajet/trajetMisAJour.php',
            'id' => $trajet->getId(),
            'trajets' => $trajets
        ]);
    }

    public static function afficherErreur(string $messageErreur = "") : void
    {
        self::afficherVue('vueGenerale.php', [
            'titre' => "Erreur trajet",
            'cheminCorpsVue' => 'trajet/erreur.php',
            'messageErreur' => $messageErreur
        ]);
    }
}