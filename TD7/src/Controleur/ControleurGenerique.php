<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\PreferenceControleur;

class ControleurGenerique
{
    public static function enregistrerPreference() {
        $controleur = $_GET["controleur_defaut"];
        PreferenceControleur::enregistrer($controleur);
        self::afficherVue('vueGenerale.php', [
            'titre' => 'Preference enregistre',
            'cheminCorpsVue' => 'preferenceEnregistree.php'
        ]);
    }

    public static function afficherFormulairePreference() {
        self::afficherVue('vueGenerale.php', [
            'titre' => 'Formulaire preference',
            'cheminCorpsVue' => 'formulairePreference.php'
        ]);
    }

    public static function afficherErreur(string $messageErreur = "") : void
    {
        self::afficherVue('vueGenerale.php', [
            'titre' => 'Erreur générique',
            'cheminCorpsVue' => 'erreur.php',
            'messageErreur' => $messageErreur
        ]);
    }

    protected static function afficherVue(string $cheminVue, array $parametres = []) : void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }
}