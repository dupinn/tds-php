<?php

namespace App\Covoiturage\Configuration;

class ConfigurationSite
{
    static private array $configurationSite = array(
        'dureeExpirationSession' => 1800
    );

    static public function getDureeExpiration() : int {
        return ConfigurationSite::$configurationSite['dureeExpirationSession'];
    }
}