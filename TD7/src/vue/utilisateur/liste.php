<?php
use App\Covoiturage\Modele\DataObject\Utilisateur;

/** @var Utilisateur[] $utilisateurs */

foreach ($utilisateurs as $utilisateur) {
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    $loginURL = rawurldecode($utilisateur->getLogin());
    echo 'Utilisateur de login ' . $loginHTML;
    echo '<span style="margin-right: 40px"></span><a style="color: green" href="http://localhost/tds-php/TD7/web/controleurFrontal.php?controleur=utilisateur&action=afficherDetail&login=' . $loginURL    . '" > Afficher détail </a>';
    echo '<span style="margin-right: 40px"></span><a style="color: crimson" href="http://localhost/tds-php/TD7/web/controleurFrontal.php?controleur=utilisateur&action=supprime&login=' . $loginURL    . '" > Supprimer </a>';
    echo '<span style="margin-right: 40px"></span><a style="color: orange" href="http://localhost/tds-php/TD7/web/controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=' . $loginURL    . '" > Mettre à jour </a>';
    echo "<hr>";
}
echo '<a style="color: darkviolet" href="http://localhost/tds-php/TD7/web/controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireCreation" >  Créer utilisateur </a>';