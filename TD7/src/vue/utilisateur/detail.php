<?php

use App\Covoiturage\Modele\DataObject\Utilisateur;

/** @var Utilisateur $utilisateur */
$loginHTML = htmlspecialchars($utilisateur->getLogin());
$prenomHTML = htmlspecialchars($utilisateur->getPrenom());
$nomHTML = htmlspecialchars($utilisateur->getNom());
echo '<p> Utilisateur de login ' . $loginHTML . ' de nom ' . $nomHTML . ' et de prénom ' . $prenomHTML .'.</p>';
echo '<p>Il sera passager sur les trajet(s) :</p>';
/** @var Trajet[] $trajets */
$trajets = $utilisateur->getTrajetsCommePassager();
foreach ($trajets as $trajet) {
    if (is_object($trajet)) {
        $idHTML = htmlspecialchars($trajet->getId());
        $departHTML = htmlspecialchars($trajet->getDepart());
        $arriveeHTML = htmlspecialchars($trajet->getArrivee());
        echo '<p>- Trajet qui a comme ID ' . $idHTML . ', qui aura comme depart ' . $departHTML . ' et comme arrivee ' . $arriveeHTML .'.</p>';
    } else {
        var_dump($trajet); // Ajoute ce code pour voir ce que contient réellement la variable.
        echo "Erreur : Le trajet n'est pas un objet.";
    }
}