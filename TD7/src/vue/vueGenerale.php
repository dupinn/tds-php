<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../ressources/css/style.css">
        <title>
            <?php
            /**
             * @var string $titre
             */
            echo $titre;
            ?>
        </title>
    </head>
    <body>
        <header>
            <nav>
                <ul>
                    <li>
                        <a href="controleurFrontal.php?controleur=utilisateur&action=afficherListe">Gestion des utilisateurs</a>
                    </li>
                    <li>
                        <a href="controleurFrontal.php?controleur=trajet&action=afficherListe">Gestion des trajets</a>
                    </li>
                    <li>
                        <a href="controleurFrontal.php?action=afficherFormulairePreference">
                            <img src="../ressources/img/heart.png" alt="heart">
                        </a>
                    </li>
                </ul>
            </nav>
        </header>
        <main>
            <?php
            /**
             * @var string $cheminCorpsVue
             */
            require __DIR__ . "/{$cheminCorpsVue}";
            ?>
        </main>
        <footer>
            <p>
                Site de covoiturage de ...
            </p>
        </footer>
    </body>
</html>