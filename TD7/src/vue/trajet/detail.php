<?php
use App\Covoiturage\Modele\DataObject\Utilisateur;

/** @var Trajet $trajet */
$idHTML = htmlspecialchars($trajet->getId());
$departHTML = htmlspecialchars($trajet->getDepart());
$arriveeHTML = htmlspecialchars($trajet->getArrivee());
$dateHTML = htmlspecialchars($trajet->getDate()->format('Y-m-d'));
$prixHTML = htmlspecialchars($trajet->getPrix());
$conducteurLoginHTML = htmlspecialchars($trajet->getConducteur()->getLogin());
$nonFumeurHTML = htmlspecialchars($trajet->isNonFumeur() ? 'fumeur' : 'non fumeur');
echo '<p>Trajet qui a comme ID ' . $idHTML . ', qui a comme ville de départ ' . $departHTML . " et comme ville d'arrivee " . $arriveeHTML . '.</p>';
echo '<p>Le trajet partira le ' . $dateHTML . ' et aura comme prix ' . $prixHTML . '€, le conducteur ayant comme login ' . $conducteurLoginHTML . '. Le trajet sera ' . $nonFumeurHTML . '.</p>';
echo '<p>Il aura comme passager(s) :</p>';
/** @var Utilisateur[] $passagers */
$passagers = $trajet->getPassagers();
foreach ($passagers as $passager) {
    if (is_object($passager)) {
        $loginHTML = htmlspecialchars($passager->getLogin());
        $prenomHTML = htmlspecialchars($passager->getPrenom());
        $nomHTML = htmlspecialchars($passager->getNom());
        echo '<p>- Utilisateur de login ' . $loginHTML . ' de nom ' . $nomHTML . ' et de prénom ' . $prenomHTML .'.</p>';
    } else {
        var_dump($passager); // Ajoute ce code pour voir ce que contient réellement la variable.
        echo "Erreur : Le passager n'est pas un objet.";
    }
}