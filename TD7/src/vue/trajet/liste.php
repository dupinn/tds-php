<?php
/** @var Trajet[] $trajets */
foreach ($trajets as $trajet) {
    $idHTML = htmlspecialchars($trajet->getId());
    $idURL = rawurldecode($trajet->getId());
    echo '<p>Trajet de id ' . $idHTML;
    echo '<span style="margin-right: 40px"></span><a style="color: green" href="http://localhost/tds-php/TD7/web/controleurFrontal.php?controleur=trajet&action=afficherDetail&id=' . $idURL . '" > Afficher détail </a>';
    echo '<span style="margin-right: 40px"></span><a style="color: crimson" href="http://localhost/tds-php/TD7/web/controleurFrontal.php?controleur=trajet&action=supprime&id=' . $idURL . '" > Supprimer </a>';
    echo '<span style="margin-right: 40px"></span><a style="color: orange" href="http://localhost/tds-php/TD7/web/controleurFrontal.php?controleur=trajet&action=afficherFormulaireMiseAJour&id=' . $idURL . '" > Mettre à jour </a>';
    echo '<span style="margin-right: 40px"></span><a style="color: blue" href="http://localhost/tds-php/TD7/web/controleurFrontal.php?controleur=passager&action=afficherFormulaireCreation&id=' . $idURL . '" > Inscrire passager </a></p>';
    echo '<ul>';
    foreach($trajet->getPassagers() as $passager){
        $loginHTML = htmlspecialchars($passager->getLogin());
        $loginURL = rawurldecode($passager->getLogin());
        $stringPassager = '<span style="margin-right: 10px"> Passager ' . $loginHTML . ' </span><a style="color: crimson" href="http://localhost/tds-php/TD7/web/controleurFrontal.php?controleur=passager&action=supprimer&id=' . $idURL . '&login=' . $loginURL . '" > Désinscrire </a>';
        echo '<li>' . $stringPassager . '</li>';
    }
    echo "</ul><hr>";
}
echo '<a style="color: darkviolet" href="http://localhost/tds-php/TD7/web/controleurFrontal.php?controleur=trajet&action=afficherFormulaireCreation" >  Créer trajet </a>';