<?php
    require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

    use App\Covoiturage\Controleur\ControleurGenerique;
    use App\Covoiturage\Lib\PreferenceControleur;

    // initialisation en activant l'affichage de débogage
    $chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
    $chargeurDeClasse->register();
    // enregistrement d'une association "espace de nom" → "dossier"
    $chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

    // On récupère le controleur passée dans l'URL
    $controleur = $_GET['controleur'] ?? (PreferenceControleur::lire() ?: 'utilisateur');

    // On récupère le nom de la classe du controleur
    $nomDeClasseControleur = 'App\Covoiturage\Controleur\Controleur' . ucfirst($controleur);
    if (class_exists($nomDeClasseControleur)) {
        $action = $_GET['action'] ?? 'afficherListe';
        if (!in_array($action, get_class_methods($nomDeClasseControleur))) {
            $action = 'afficherErreur';
        }

        $boolLogin = isset($_GET['login']);
        $boolId = isset($_GET['id']);
        $boolNom = isset($_GET['nom']);
        $boolValeur = isset($_GET['valeur']);

        if ($boolNom && $boolValeur) {
            $nom = $_GET['nom'];
            $valeur = $_GET['valeur'];
            $nomDeClasseControleur::$action($nom, $valeur);
        } else if ($boolNom) {
            $nom = $_GET['nom'];
            $nomDeClasseControleur::$action($nom);
        } else if ($boolId && $boolLogin) {
            $id = $_GET['id'];
            $login = $_GET['login'];
            $nomDeClasseControleur::$action($id, $login);
        }else if ($boolLogin) {
            $login = $_GET['login'];
            $nomDeClasseControleur::$action($login);
        } else if ($boolId) {
            $id = $_GET['id'];
            $nomDeClasseControleur::$action($id);
        } else {
            $nomDeClasseControleur::$action();
        }
    } else {
        ControleurGenerique::afficherErreur("Ce controleur n'existe pas");
    }