<?php
    require_once ('../Modele/ModeleUtilisateur.php'); // chargement du modèle
    class ControleurUtilisateur {
        // Déclaration de type de retour void : la fonction ne retourne pas de valeur
        public static function afficherListe() : void {
            $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
            self::afficherVue('utilisateur/liste.php', [
                'utilisateurs' => $utilisateurs
            ]);
        }

        public static function afficherDetail($login) : void {
            $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($login); //appel au modèle pour gérer la BD
            if (isset($utilisateur)) {
                self::afficherVue('utilisateur/detail.php', [
                    "utilisateur" => $utilisateur
                ]);
            } else {
                self::afficherVue('utilisateur/erreur.php');
            }
        }

        public static function afficherFormulaireCreation() : void {
            self::afficherVue('utilisateur/formulaireCreation.php');
        }

        public static function creerDepuisFormulaire($login, $nom, $prenom) : void {
            $utilisateur = new ModeleUtilisateur($login, $nom, $prenom);
            $utilisateur->ajouter();
            self::afficherListe();
        }

        private static function afficherVue(string $cheminVue, array $parametres = []) : void {
            extract($parametres); // Crée des variables à partir du tableau $parametres
            require "../vue/$cheminVue"; // Charge la vue
        }
    }
?>