<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Liste des utilisateurs</title>
</head>
    <body>
        <?php
            /** @var ModeleUtilisateur $utilisateur */
            echo '<p> Utilisateur de login ' . $utilisateur->getLogin() . ' de nom ' . $utilisateur->getNom() . ' et de prénom ' . $utilisateur->getPrenom() .'.</p>';
        ?>
    </body>
</html>