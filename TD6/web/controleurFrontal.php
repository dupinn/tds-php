<?php
    require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

    use App\Covoiturage\Controleur\ControleurTrajet;
    use App\Covoiturage\Controleur\ControleurUtilisateur;
    use App\Covoiturage\Controleur\ControleurPassager;

    // initialisation en activant l'affichage de débogage
    $chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
    $chargeurDeClasse->register();
    // enregistrement d'une association "espace de nom" → "dossier"
    $chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

    // On récupère le controleur passée dans l'URL
    $controleur = $_GET['controleur'] ?? 'utilisateur';

    // On récupère le nom de la classe du controleur
    $nomDeClasseControleur = 'App\Covoiturage\Controleur\Controleur' . ucfirst($controleur);
    if (class_exists($nomDeClasseControleur)) {
        if ($controleur == 'utilisateur') {
            $action = $_GET['action'] ?? 'afficherListe';
            if (!in_array($action, get_class_methods(App\Covoiturage\Controleur\ControleurUtilisateur::class))) {
                $action = 'afficherErreur';
            }

            $boolLogin = isset($_GET['login']);

            if ($boolLogin) {
                $login = $_GET['login'];
                ControleurUtilisateur::$action($login);
            } else {
                ControleurUtilisateur::$action();
            }
        } else if ($controleur == 'trajet') {
            $action = $_GET['action'] ?? 'afficherListe';
            if (!in_array($action, get_class_methods(App\Covoiturage\Controleur\ControleurTrajet::class))) {
                $action = 'afficherErreur';
            }

            $boolId = isset($_GET['id']);
            $boolLogin = isset($_GET['login']);
            if ($boolId && $boolLogin) {
                $id = $_GET['id'];
                $login = $_GET['login'];
                ControleurTrajet::$action($id, $login);
            } else if ($boolId) {
                $id = $_GET['id'];
                ControleurTrajet::$action($id);
            } else {
                ControleurTrajet::$action();
            }
        } else if ($controleur == 'passager') {
            $action = $_GET['action'] ?? 'afficherListe';
            if (!in_array($action, get_class_methods(App\Covoiturage\Controleur\ControleurPassager::class))) {
                $action = 'afficherErreur';
            }

            $boolId = isset($_GET['id']);
            $boolLogin = isset($_GET['login']);
            if ($boolId && $boolLogin) {
                $id = $_GET['id'];
                $login = $_GET['login'];
                ControleurPassager::$action($id, $login);
            }
            else if ($boolId) {
                $id = $_GET['id'];
                ControleurPassager::$action($id);
            } else {
                ControleurTrajet::$action();
            }
        }

    } else {
        ControleurUtilisateur::afficherErreur("Ce controleur n'existe pas");
    }
?>
