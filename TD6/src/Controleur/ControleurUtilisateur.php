<?php
    namespace App\Covoiturage\Controleur;

    use App\Covoiturage\Modele\Repository\UtilisateurRepository;
    use App\Covoiturage\Modele\DataObject\Utilisateur;

    class ControleurUtilisateur
    {
        // Déclaration de type de retour void : la fonction ne retourne pas de valeur
        public static function afficherListe(): void
        {
            $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
            self::afficherVue('vueGenerale.php', [
                'titre' => "Liste des utilisateurs",
                'cheminCorpsVue' => 'utilisateur/liste.php',
                'utilisateurs' => $utilisateurs
            ]);
        }

        public static function afficherDetail($login): void
        {
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login); //appel au modèle pour gérer la BD
            if (isset($utilisateur)) {
                self::afficherVue('vueGenerale.php', [
                    'titre' => "Detail utilisateur",
                    'cheminCorpsVue' => 'utilisateur/detail.php',
                    "utilisateur" => $utilisateur
                ]);
            } else {
                self::afficherErreur("Cette utilisateur n'existe pas");
            }
        }

        public static function afficherFormulaireCreation(): void
        {
            self::afficherVue('vueGenerale.php', [
                'titre' => "Formulaire de création utilisateur",
                'cheminCorpsVue' => 'utilisateur/formulaireCreation.php',
            ]);
        }

        public static function afficherFormulaireMiseAJour($login): void
        {
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
            if (isset($utilisateur)) {
                self::afficherVue('vueGenerale.php', [
                    'titre' => "Formulaire de mise à jour utilisateur",
                    'cheminCorpsVue' => 'utilisateur/formulaireMiseAJour.php',
                    'utilisateur' => $utilisateur
                ]);
            } else {
                self::afficherErreur("Cette utilisateur n'existe pas");
            }
        }

        private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur {
            return new Utilisateur($tableauDonneesFormulaire['login'],
                $tableauDonneesFormulaire['nom'],
                $tableauDonneesFormulaire['prenom'],
            );
        }

        public static function creerDepuisFormulaire(): void
        {
            $utilisateur = self::construireDepuisFormulaire($_GET);
            (new UtilisateurRepository())->ajouter($utilisateur);
            $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
            self::afficherVue('vueGenerale.php', [
                'titre' => "Liste des utilisateurs",
                'cheminCorpsVue' => 'utilisateur/utilisateurCree.php',
                'utilisateurs' => $utilisateurs
            ]);
        }

        public static function supprime($login): void
        {
            (new UtilisateurRepository())->supprimer($login);
            $utilisateurs = (new UtilisateurRepository())->recuperer();
            self::afficherVue('vueGenerale.php', [
                'titre' => "Liste des utilisateurs",
                'cheminCorpsVue' => 'utilisateur/utilisateurSupprime.php',
                'login' => $login,
                'utilisateurs' => $utilisateurs
            ]);
        }

        public static function mettreAJour(): void
        {
            $utilisateur = self::construireDepuisFormulaire($_GET);
            (new UtilisateurRepository())->mettreAJour($utilisateur);
            $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
            self::afficherVue('vueGenerale.php', [
                'titre' => "Liste des utilisateurs",
                'cheminCorpsVue' => 'utilisateur/utilisateurMisAJour.php',
                'login' => $utilisateur->getLogin(),
                'utilisateurs' => $utilisateurs
            ]);
        }

        public static function afficherErreur(string $messageErreur = "") : void
        {
            self::afficherVue('vueGenerale.php', [
                'titre' => "Erreur utilisateur",
                'cheminCorpsVue' => 'utilisateur/erreur.php',
                'messageErreur' => $messageErreur
            ]);
        }

        private static function afficherVue(string $cheminVue, array $parametres = []) : void
        {
            extract($parametres); // Crée des variables à partir du tableau $parametres
            require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
        }
    }
?>