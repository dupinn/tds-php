<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Passager;
use App\Covoiturage\Modele\Repository\PassagerRepository;
use App\Covoiturage\Modele\Repository\TrajetRepository;

class ControleurPassager
{
    public static function afficherFormulaireCreation($id): void
    {
        $trajet = (new TrajetRepository())->recupererParClePrimaire($id);
        if (isset($trajet)) {
            self::afficherVue('vueGenerale.php', [
                'titre' => "Formulaire d'inscription passager",
                'cheminCorpsVue' => 'passager/formulaireCreation.php',
                'trajet' => $trajet
            ]);
        } else {
            self::afficherErreur("Ce trajet n'existe pas");
        }
    }

    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Passager {
        return new Passager($tableauDonneesFormulaire['id'],
            $tableauDonneesFormulaire['login']
        );
    }

    public static function creerDepuisFormulaire(): void
    {
        $passager = self::construireDepuisFormulaire($_GET);
        (new PassagerRepository())->ajouter($passager);
        $trajets = (new TrajetRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', [
            'titre' => "Liste des trajets",
            'cheminCorpsVue' => 'passager/passagerCree.php',
            'passager' => $passager,
            'trajets' => $trajets
        ]);
    }

    public static function supprimer($id, $login): void
    {
        $clePrimaire = $id . "," . $login;
        $passager = (new PassagerRepository())->recupererParClePrimaire($clePrimaire);
        (new PassagerRepository())->supprimer($clePrimaire);
        $trajets = (new TrajetRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', [
            'titre' => "Liste des trajets",
            'cheminCorpsVue' => 'passager/passagerSupprime.php',
            'passager' => $passager,
            'trajets' => $trajets
        ]);
    }

    public static function afficherErreur(string $messageErreur = "") : void
    {
        self::afficherVue('vueGenerale.php', [
            'titre' => "Erreur passager",
            'cheminCorpsVue' => 'passager/erreur.php',
            'messageErreur' => $messageErreur
        ]);
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }
}