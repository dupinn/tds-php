<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;

class UtilisateurRepository extends AbstractRepository
{
    protected function getNomTable(): string
    {
        return 'utilisateur';
    }

    protected function getNomClePrimaire(): string
    {
        return 'login';
    }

    protected function getNomsColonnes(): array
    {
        return ["login", "nom", "prenom"];
    }

    public function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Utilisateur
    {
        return new Utilisateur($utilisateurFormatTableau['login'], $utilisateurFormatTableau['nom'], $utilisateurFormatTableau['prenom']);
    }

    protected function formatTableauSQL(AbstractDataObject $utilisateur): array
    {
        /** @var Utilisateur $utilisateur */
        return array(
            "loginTag" => $utilisateur->getLogin(),
            "nomTag" => $utilisateur->getNom(),
            "prenomTag" => $utilisateur->getPrenom(),
        );
    }

    /** @var Utilisateur $utilisateur
     * @return Trajet[]
     */
    public function recupererTrajetsCommePassager(AbstractDataObject $utilisateur) : array
    {
        $sql = "SELECT * FROM trajet t JOIN passager p ON t.id = p.trajetId WHERE p.passagerLogin = :loginTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = [
            "loginTag" => $utilisateur->getLogin(),
        ];
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);
        $tableau = [];
        foreach($pdoStatement as $objetFormatTableau){
            $tableau[] = (new TrajetRepository())->construireDepuisTableauSQL($objetFormatTableau);
        }
        return $tableau;
    }
}