<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use PDOException;

abstract class AbstractRepository
{
    protected abstract function getNomTable(): string;

    protected abstract function getNomClePrimaire(): string;

    protected abstract function getNomsColonnes(): array;

    protected abstract function construireDepuisTableauSQL(array $objetTableau) : AbstractDataObject;

    protected abstract function formatTableauSQL(AbstractDataObject $objet): array;

    protected function construireTagClePrimaire(): string {
        $nomsClesPrimaire = explode(',', $this->getNomClePrimaire());
        $tabTag = [];

        foreach ($nomsClesPrimaire as $clePrimaire) {
            $tabTag[] = "$clePrimaire = :$clePrimaire" . "Tag";
        }

        return implode(' AND ', $tabTag);
    }

    /**
     * @return AbstractDataObject[]
     */
    public function recuperer() : array
    {
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM " . $this->getNomTable());
        $tableau = [];
        foreach($pdoStatement as $objetFormatTableau){
            $tableau[] = $this->construireDepuisTableauSQL($objetFormatTableau);
        }
        return $tableau;
    }

    public function recupererParClePrimaire(string $clePrimaire): ?AbstractDataObject
    {
        $sql = "SELECT * FROM " . $this->getNomTable() . " WHERE " . $this->construireTagClePrimaire();
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $tabClePrimaire = explode(',', $this->getNomClePrimaire());
        $tabValeurClePrimaire = explode(',', $clePrimaire);
        $values = [];
        foreach ($tabClePrimaire as $index => $clePrimaire) {
            $valeurClePrimaire = $tabValeurClePrimaire[$index];
            $values["$clePrimaire" . "Tag"] = $valeurClePrimaire;
        }

        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $objetFormatTableau = $pdoStatement->fetch();
        if (!$objetFormatTableau) {
            return null;
        }
        return $this->construireDepuisTableauSQL($objetFormatTableau);
    }

    public function ajouter(AbstractDataObject $objet): bool
    {
        $sql = "INSERT INTO " . $this->getNomTable() . " (" . implode(', ', $this->getNomsColonnes()) . ") VALUES (:" . implode('Tag, :', $this->getNomsColonnes()) . "Tag)";
        // Préparation de la requête
        try {
            $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        } catch(PDOException $Exception) {
            return false;
        }

        $values = $this->formatTableauSQL($objet);
        // On donne les valeurs et on exécute la requête
        return $pdoStatement->execute($values);
    }

    public function supprimer($clePrimaire): void
    {
        $sql = "DELETE FROM " . $this->getNomTable() . " WHERE " . $this->construireTagClePrimaire();
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $tabClePrimaire = explode(',', $this->getNomClePrimaire());
        $tabValeurClePrimaire = explode(',', $clePrimaire);
        $values = [];
        foreach ($tabClePrimaire as $index => $clePrimaire) {
            $valeurClePrimaire = $tabValeurClePrimaire[$index];
            $values["$clePrimaire" . "Tag"] = $valeurClePrimaire;
        }

        $pdoStatement->execute($values);
    }

    public function mettreAJour(AbstractDataObject $objet): void
    {
        $tabSet = [];
        foreach ($this->getNomsColonnes() as $colonne) {
            if (strcmp($colonne, $this->getNomClePrimaire()) !== 0) {
                $tabSet[] = $colonne . " = :" . $colonne . "Tag";
            }
        }
        $sql = "UPDATE " . $this->getNomTable() . " SET " . implode(', ', $tabSet) . " WHERE " . $this->getNomClePrimaire() . " = :" . $this->getNomClePrimaire() . "Tag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = $this->formatTableauSQL($objet);
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);
    }
}