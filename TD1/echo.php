<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>

    <body>
        Voici le résultat du script PHP :
        <?php
            $utilisateur1 = [
                'nom' => 'PIQUO',
                'prenom' => 'GERARD',
                'login' => 'PIQUOG'
            ];

            $utilisateur2 = [
                'nom' => 'PIQUE',
                'prenom' => 'JOSEPH',
                'login' => 'PIQUEJ'
            ];

            $utilisateurs = [
                1 => $utilisateur1,
                2 => $utilisateur2
            ];
            echo "<h1>Liste des utilisateurs</h1>";
            if (empty($utilisateurs)) {
                echo "Il n'y a aucun utilisateur";
            } else {
                echo "<ul>";
                foreach ($utilisateurs as $key => $value) {
                    echo "<li>";
                    foreach ($value as $key1 => $value1) {
                        echo $key1 . ": " . $value1 . " ; ";
                    }
                    echo "</li>";
                }
                echo "</ul>";
            }
        ?>
    </body>
</html> 